# Auto Parts

`Auto Parts` is a inventory control system which can be used to keep inventory tracks in a auto parts selling store.

## Getting started
Project has been developed using Netbeans IDE, therefor project needs to be opened from Netbeans IDE.  

## Prerequisite

User should have following requirements to run the project successfully.

* Mysql version 5.6.40 or above

* Database for auto parts

* Tables created to save the data

\* Database script can be obtained by sending an email to the **niroshan.kdt@gmail.com**.

### Third party libraries

Please add the following libraries to the project's build path.

* jcalender-1.3.2

* seaglasslookandfeel-0.2

* VistaCalender

* WebLookAndFeel

* jgoodies-looks-2.5.3

* reference.Buoy