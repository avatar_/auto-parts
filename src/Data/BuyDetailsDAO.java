/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import java.sql.Connection;
import java.sql.Statement;
import java.util.Vector;

/**
 *
 * @author Niroshan
 */
public class BuyDetailsDAO {
    DBConnManager dbconn;
    
    public BuyDetailsDAO()
    {
        dbconn = new DBConnManager();
    }
    
    public boolean saveBuyDetails(BuyDetails pBuy)
    {
        boolean saved = false;
        
        Connection conn = null;
        try {
            conn = dbconn.getConnect();
            Statement stmt = conn.createStatement();
            
            Vector details = pBuy.getBuyDetails();
            
            for (int i = 0 ; i < details.size() ; i++ ) {
                Vector subDet = (Vector) details.elementAt(i);
                
                int buy_id = Integer.parseInt(subDet.elementAt(0).toString());
                String itemID = subDet.elementAt(1).toString();
                double quantity = Double.parseDouble(subDet.elementAt(2).toString());
                
                String sql = "INSERT INTO buy_det(buy_id,item_id,quantity) "
                        + "VALUES ("+buy_id+" ,'"+itemID+"',"+quantity+" )";
                System.out.println(sql);
                
                stmt.addBatch(sql);
               
            }
            
            int []k = stmt.executeBatch();
            
            if(k[0]>0)
            {
                saved = true;
            }
            
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            dbconn.closeConnection(conn);
        }
        
        
        
        return saved;
        
    }
}
