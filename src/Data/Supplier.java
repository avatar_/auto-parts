/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

/**
 *
 * @author Niroshan
 */
public class Supplier
{
   private String supplier_ID;
   private String supplier_name;
   private String telephone;

    public Supplier(String pSupplier_id,String pSupplier_name,String pTelephone)
    {
        this.supplier_ID = pSupplier_id;
        this.supplier_name = pSupplier_name;
        this.telephone = pTelephone;
    }
    public String getSupplier_ID() {
        return supplier_ID;
    }

    public void setSupplier_ID(String supplier_ID) {
        this.supplier_ID = supplier_ID;
    }

    public String getSupplier_name() {
        return supplier_name;
    }

    public void setSupplier_name(String supplier_name) {
        this.supplier_name = supplier_name;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
   
   
    
    
}
