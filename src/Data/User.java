/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

/**
 *
 * @author Niroshan
 */
public class User {
    private String userID;
    private String f_name;
    private String l_name;
    private String password;
    
    public User(String pUID,String pF_name,String pL_name,String  pPassword)
    {
        this.f_name = pF_name;
        this.l_name = pL_name;
        this.password = pPassword;
        this.userID = pUID;
        
    }
    public User(String pUID,String pF_name,String pL_name )
    {
        this.f_name = pF_name;
        this.l_name = pL_name;
        this.userID = pUID;
        
    }
    
    public User(String pUserID,String pPassword)
    {
        this.userID = pUserID;
        this.password = pPassword;        
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getF_name() {
        return f_name;
    }

    public void setF_name(String f_name) {
        this.f_name = f_name;
    }

    public String getL_name() {
        return l_name;
    }

    public void setL_name(String l_name) {
        this.l_name = l_name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    

    
}
