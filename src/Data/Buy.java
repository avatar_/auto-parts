/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author Niroshan
 */
public class Buy {
    private int buyID;
    private String userID;
    private String date;
    private String supplierID;
    
    public Buy(int pBID,String puserID,String pSupplierID)
    {
        this.buyID =pBID;
        this.supplierID = pSupplierID;
        this.userID = puserID;
    }

    public int getBuyID() {
        return buyID;
    }

    public void setBuyID(int buyID) {
        this.buyID = buyID;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getDate() {
         SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance(); 
        return date = df.format(cal.getTime());
    }

    public void setDate(String date) {
        this.date = date;
    }
    public String getSupplierID() {
        return supplierID;
    }

    public void setSupplierID(String supplierID) {
        this.supplierID = supplierID;
    }
    
    
    
}
