/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import com.toedter.calendar.JDateChooser;
import java.util.Calendar;

/**
 *
 * @author Niroshan
 */
public class DatePicker 
{
    public static String getDate(JDateChooser ch)
    {
        Calendar c = ch.getCalendar();
        String year = Integer.toString(c.get(Calendar.YEAR));
        String month = Integer.toString(c.get(Calendar.MONTH)+1);
        String day = Integer.toString(c.get(Calendar.DAY_OF_MONTH));
        
         String date = year+"-"+month+"-"+day ;
 
         return date;
    }
    
}
