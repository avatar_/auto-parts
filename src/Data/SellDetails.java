/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

/**
 *
 * @author Niroshan
 */
public class SellDetails {

    private int jobID;
    private String itemID;
    private int quantity;
    
    public SellDetails(int pJID,String pIID,int pQuantity)
    {
        this.itemID = pIID;
        this.jobID = pJID;
        this.quantity = pQuantity;
        
    }
    
     public int getJobID() {
        return jobID;
    }

    public void setJobID(int jobID) {
        this.jobID = jobID;
    }

    public String getItemID() {
        return itemID;
    }

    public void setItemID(String itemID) {
        this.itemID = itemID;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

}
