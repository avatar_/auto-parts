/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

/**
 *
 * @author Niroshan
 */
public class Customer {
    private String cusID;
    private String fname;
    private String lname;

    public Customer(String pCusID, String pFname, String pLname)
    {
        this.cusID =pCusID;
        this.fname = pFname;
        this.lname = pLname;
    }
    
    public String getCusID() {
        return cusID;
    }

    public void setCusID(String cusID) {
        this.cusID = cusID;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }
    
}
