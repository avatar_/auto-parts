/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

/**
 *
 * @author Niroshan
 */
public class BorrowDAO {
    
    DBConnManager dbconn;
    
    public BorrowDAO()
    {
        dbconn = new DBConnManager();
    }
    
    public boolean saveBorrow(Borrow bar)
    {
        boolean saved= false;
        Connection conn = null;
        
        try 
        {
            int jobID = bar.getJobID();
            String cus = bar.getCustommerID();
            double amnt = bar.getAmount();
            double paid = bar.getPaid();
            
            conn = dbconn.getConnect();
            Statement stmt = conn.createStatement();
            String sql = "INSERT INTO borrow(jobID,customerID,amount,paid)"
                    + "VALUES ("+jobID+",'"+cus+"','"+amnt+"','"+paid+"')";
            int i = stmt.executeUpdate(sql);
            
            if(i >= 1)
            {
                saved = true;
            }
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        finally
        {
            dbconn.closeConnection(conn);
        }
        return saved;
        
    }
    
    public Vector getAllBorrowingDetails()
    {
        Vector borrowingDetails = new Vector();
        Connection conn = null;
        
        try 
        {
            conn = dbconn.getConnect();
            Statement stmt = conn.createStatement();
            String sql = "SELECT jobID,customerID,amount,paid FROM borrow";
            ResultSet rst = stmt.executeQuery(sql);
            
            while(rst.next())
            {
                Vector v = new Vector();
                v.add(rst.getString(1));
                v.add(rst.getString(2));
                v.add(rst.getString(3));
                v.add(rst.getString(4));
                
                borrowingDetails.add(v);
            }
       
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            dbconn.closeConnection(conn);
        }

        return borrowingDetails;
        
    }
    
    
    
}
