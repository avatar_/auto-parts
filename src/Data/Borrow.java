/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

/**
 *
 * @author Niroshan
 */
public class Borrow {
    private int jobID;
    private String custommerID;
    private double amount;
    private double paid;
    
    public Borrow(int pJobID,String pCusID,double pAmount, double pPaid)
    {
       this.jobID = pJobID;
       this.custommerID = pCusID;
       this.amount = pAmount;
       this.paid = pPaid;
    }

    public int getJobID() {
        return jobID;
    }

    public void setJobID(int jobID) {
        this.jobID = jobID;
    }

    public String getCustommerID() {
        return custommerID;
    }

    public void setCustommerID(String custommerID) {
        this.custommerID = custommerID;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getPaid() {
        return paid;
    }

    public void setPaid(double paid) {
        this.paid = paid;
    }
}
