/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

/**
 *
 * @author Niroshan
 */
public class Sell 
{
    private String jobID;
    private String userID;
    private String date;
    
    public Sell(String pJID, String pUID, String pDate)
    {
        this.date = pDate;
        this.jobID = pJID;
        this.userID = pUID;
        
    }
     public Sell(String pJID,String pUID )
    {         
        this.jobID = pJID;   
        this.userID = pUID;
    }

    public String getJobID() {
        return jobID;
    }

    public void setJobID(String jobID) {
        this.jobID = jobID;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
    
    
}
