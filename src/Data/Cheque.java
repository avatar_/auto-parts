/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import java.math.BigDecimal;

/**
 *
 * @author Niroshan
 */
public class Cheque 
{
    private String chequeID;
    private BigDecimal amount;
    private String description;
    private String dueDate;
    private String date;
    private String userID;

    public  Cheque(String pCHID,BigDecimal pamt,String pdescription,String pDueDate,String puser)
    {
        this.chequeID = pCHID;
        this.amount = pamt;
        this.description = pdescription;
        this.dueDate = pDueDate;
        this.userID = puser;
        
    }
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getChequeID() {
        return chequeID;
    }

    public void setChequeID(String chequeID) {
        this.chequeID = chequeID;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }
    
}
