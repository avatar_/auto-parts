/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

/**
 *
 * @author Niroshan
 */
public class CustomerDAO {
    DBConnManager dbconn;
    
    public CustomerDAO()
    {
        dbconn = new DBConnManager();
    }
    
    public boolean existingCustomer(String cusID)
    {
        boolean existing = false;

        Connection conn = null;

        try {
            conn = dbconn.getConnect();
            Statement stmt = conn.createStatement();

            String sql = "SELECT EXISTS (SELECT customerID FROM customer WHERE customerID = '" + cusID + "' AND isDeleted ='0')";
            ResultSet rst = stmt.executeQuery(sql);
            rst.next();

            if (Integer.parseInt(rst.getString(1)) == 1) {
                existing = true;
            }

        } catch (Exception e) {
            System.out.println(e);
        } finally
        {
            dbconn.closeConnection(conn);
        }
        return existing;
    }
    
    public boolean saveCustomer(Customer cus)
    {
        boolean saved = false;

        Connection conn = null;

        try {
            conn = dbconn.getConnect();
            Statement stmt = conn.createStatement();

            String cusID = cus.getCusID();
            String fname = cus.getFname();
            String lname = cus.getLname();

            String sql = "INSERT INTO customer(customerID,firstName,lastName,isDeleted)"
                    + " VALUES('" + cusID + "','" + fname + "','" + lname + "','0')";
            int i = stmt.executeUpdate(sql);

            if (i >= 1) {
                saved = true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbconn.closeConnection(conn);
        }
        return saved;

    }

    public Vector getCustomerID()
    {
        Vector cusID = new Vector();
        
        Connection conn = null;
        
        try 
        {
            conn = dbconn.getConnect();
            Statement stmt = conn.createStatement();
            
            String sql = "SELECT customerID FROM customer WHERE isDeleted = '0'";
            
            ResultSet rst = stmt.executeQuery(sql);
            
            while(rst.next())
            {
                cusID.add(rst.getString(1));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbconn.closeConnection(conn);
        }

        return cusID;
    }
    
    
}
