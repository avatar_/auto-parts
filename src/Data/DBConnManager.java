/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;



/**
 *
 * @author DELL
 */ 
public class DBConnManager {

    String sourceurl;

    public DBConnManager() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            sourceurl = new String("jdbc:mysql://localhost:3306/db_motor_parts");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DBConnManager.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, ex.getException() + "-----------Unable to load database driver classes");
        }

    }

    public Connection getConnect() {
        Connection dbConn = null;
        try {
            dbConn = DriverManager.getConnection(sourceurl, "root", "root");
        } catch (SQLException ex) {
            Logger.getLogger(DBConnManager.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, ex.getNextException() + "----------DB Connection Faliure");
        }
        return dbConn;
    }

    public void closeConnection(Connection dbConn) {
        try {
            dbConn.close();
        } catch (SQLException ex) {
            Logger.getLogger(DBConnManager.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, ex.getNextException() + "----------DB Connection close Faliure");
        }
    }
}
