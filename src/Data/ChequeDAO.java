/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

/**
 *
 * @author Niroshan
 */
public class ChequeDAO
{
    DBConnManager dbconn ;
    public ChequeDAO()
    {
        dbconn = new DBConnManager();
    }
    /*
     * this method is used for insert a new cheque 
     */
    public boolean saveCheque(Cheque ch)
    {
        boolean saved = false;
        Connection conn = null;
        
        try 
        {
            conn = dbconn.getConnect();
            Statement stmt  = conn.createStatement();
            String chID = ch.getChequeID();
            BigDecimal amt = ch.getAmount();
            String des = ch.getDescription();
            String dueDt = ch.getDueDate();
            String user = ch.getUserID();
            String date = getdate();
            
            
            
            String sql ="INSERT INTO cheque(cheque_id,amount,description,due_date,date,user_id)"
                    + "VALUES('"+chID+"','"+amt+"','"+des+"','"+dueDt+"','"+date+"','"+user+"')";
            int i = stmt.executeUpdate(sql);
            
            if(i>=1)
            {
                saved = true;
            }
            
            
        }
        catch (Exception e) 
        {
            System.out.println(e);
        }
        finally
        {
            dbconn.closeConnection(conn);
        }
        return saved;
        
    }
    /*
     * this method is used for get the date in correct format from datepicker
     */
     public String getdate()
   {
       String date1 = null;
       DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	   //get current date time with Date()
	   Date date = new Date();
	   date1 = dateFormat.format(date.getTime());
 
	   //get current date time with Calendar()
       
       return date1;
   }
    /*
     * this method is used for check wheather cheque is existing or not
     */
     public boolean existingCheque(String chID)
     {
         boolean existing = false;
 
         Connection conn=null;
         try 
         {
             conn = dbconn.getConnect();
             Statement stmt = conn.createStatement();
             
             String sql = "SELECT COUNT(*) FROM cheque WHERE cheque_id = '"+chID+"' ";
             ResultSet rst = stmt.executeQuery(sql);
             
             if(rst.next() && Integer.parseInt(rst.getString(1))== 1)
             {
                 existing = true;
             }
             
         }
         catch (Exception e) 
         {
             System.out.println(e);
         }
         finally
         {
             dbconn.closeConnection(conn);
         }
         
         
         return  existing ;
     
     }
     /*
      * this method is used for delete a cheque
      */
     public boolean deleteCheque(String chID)
     {
         boolean deleted = false;
         Connection conn = null;
         
         try 
         {
             conn = dbconn.getConnect();
             Statement stmt = conn.createStatement();
             
             String sql = "DELETE FROM  cheque WHERE cheque_id = '"+chID+"'";
             int i = stmt.executeUpdate(sql);
             
             if(i >= 1)
             {
                 deleted = true;
             }
             
         }
         catch (Exception e)
         {
             System.out.println(e);
         }
         finally
         {
             dbconn.closeConnection(conn);
         }
         return deleted;
     }
     /*
      * this method is used for load the cheque details within 7 days
      */
     public Vector loadChequeDetails()
     {
        Vector chequeDetails = new Vector();
        Calendar date1 = Calendar.getInstance();

        String date = date1.get(Calendar.YEAR)+"-"+(date1.get(Calendar.MONTH)+1)+"-"+date1.get(Calendar.DATE) ; 
        date1.add(Calendar.DATE, 7) ;
        String date2 = date1.get(Calendar.YEAR)+"-"+(date1.get(Calendar.MONTH)+1)+"-"+(date1.get(Calendar.DATE));    
        Connection conn = null;    
         try 
         {
             conn = dbconn.getConnect();
             Statement stmt = conn.createStatement();
             String sql ="SELECT cheque_id,amount,description,date,due_date"
                     + " FROM cheque WHERE due_date BETWEEN '"+date+"' AND '"+date2+"' ORDER BY due_date ";
             
             ResultSet rst = stmt.executeQuery(sql);
             
             while (rst.next())
             {
                 Vector v = new Vector();
                 v.add(rst.getString(1));
                 v.add(rst.getString(2));
                 v.add(rst.getString(3));
                 v.add(rst.getString(4));
                 v.add(rst.getString(5));
                 
                 chequeDetails.add(v);
                 
             }
         }
         catch (Exception e) 
         {
             System.out.println(e);
         }
         finally
         {
             dbconn.closeConnection(conn);
         }
         return chequeDetails;
     }

     /*
      * this method is used for update cheque
      */
     public boolean updateChequeDetails(Cheque ch)
     {
         boolean updated = false;
         Connection conn = null;
         
         try 
         {
             conn = dbconn.getConnect();
             Statement stmt = conn.createStatement();
             String chID = ch.getChequeID();
             BigDecimal amt = ch.getAmount();
             String des = ch.getDescription();
             String due = ch.getDueDate();
             
             String sql ="UPDATE cheque SET amount = '"+amt+"' , description='"+des+"' ,due_date = '"+due+"' "
                     + "WHERE cheque_id = '"+chID+"'   ";
             int i = stmt.executeUpdate(sql);
             if(i >= 1)
             {
                 updated = true;
             }
         }
         catch (Exception e) 
         {
             System.out.println(e);
         }
         finally
         {
             dbconn.closeConnection(conn);
         }
         
         return updated;
     }

}
