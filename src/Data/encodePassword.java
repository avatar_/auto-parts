/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import java.io.IOException;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 *
 * @author Niroshan
 */
public class encodePassword 
{
    public String encode(String str)
    {
        BASE64Encoder encoder = new BASE64Encoder();
        str = (encoder.encodeBuffer(str.getBytes())).toString();
        return str;
    }

    public String decode(String str) 
    {
        BASE64Decoder decoder = new BASE64Decoder();
        try {
            str = new String(decoder.decodeBuffer(str));
        } catch (IOException ex) {
            System.out.println(ex);
        }
        return str;
    }
}
