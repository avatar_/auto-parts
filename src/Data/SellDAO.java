/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author Niroshan
 */
public class SellDAO {
    DBConnManager dbconn;
    public SellDAO()
    {
        dbconn = new DBConnManager();
    }
    public boolean saveSell(Sell sl) {
        boolean saved = false;
        Connection conn = null;

        try {
            conn = dbconn.getConnect();
            Statement stmt = conn.createStatement();
            String jobID = sl.getJobID();
            String userID = sl.getUserID();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Calendar cal = Calendar.getInstance();
            String dt = df.format(cal.getTime());

            String sql = "INSERT INTO sell(job_id,user_id,date) VALUES ('" + jobID + "','" + userID + "','" + dt + "')";

            int i = stmt.executeUpdate(sql);

            if (i >= 1) {
                saved = true;
            } else if (i == 0) {
                saved = false;
            }

        } catch (Exception e) {
            System.out.println(e);
        } finally {
        }
        return saved;
    }
    
    public int loadJobID() {
        int jobID = 1;

        Connection conn = null;

        try {
            conn = dbconn.getConnect();
            Statement stmt = conn.createStatement();

            String sql = "SELECT MAX(job_id) FROM sell";

            ResultSet rst = stmt.executeQuery(sql);

            if (rst.next() && rst.getString(1) != null) {
                jobID = Integer.parseInt(rst.getString(1));
                jobID++;
            } else {
                jobID = 1;
            }

        } catch (Exception e) {
            System.out.println(e);
        } finally {
            dbconn.closeConnection(conn);
        }

        return jobID;
    }
    
}
