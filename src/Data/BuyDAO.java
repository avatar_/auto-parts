/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author Niroshan
 */
public class BuyDAO {
    DBConnManager dbconn;
    
    public BuyDAO()
    {
        dbconn = new DBConnManager();
    }
    
    public boolean saveBuy(Buy bu)
    {
        boolean saved = false;
        Connection conn = null;
        
        try 
        {
            conn = dbconn.getConnect();
            Statement stm = conn.createStatement();
            
            int buy_id = bu.getBuyID();
            String user_id = bu.getUserID();
            String supplier_id = bu.getSupplierID();
            String date = bu.getDate();
            
            String sql ="INSERT INTO buy(buy_id,user_id,date,supplier_id)"
                    + "VALUES('"+buy_id+"','"+user_id+"','"+date+"','"+supplier_id+"')";
            
            int i = stm.executeUpdate(sql);
            
            if(i > 0)
            {
                saved = true;
            }
            else if(i <= 0  )
            {
                saved = false;
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally{
            dbconn.closeConnection(conn);
        }
        
        return  saved;
    }
    
     public int loadJobID() {
        int jobID = 1;

        Connection conn = null;

        try {
            conn = dbconn.getConnect();
            Statement stmt = conn.createStatement();

            String sql = "SELECT MAX(buy_id) FROM buy";

            ResultSet rst = stmt.executeQuery(sql);

            if (rst.next() && rst.getString(1) != null) {
                jobID = Integer.parseInt(rst.getString(1));
                jobID++;
            } else {
                jobID = 1;
            }

        } catch (Exception e) {
            System.out.println(e);
        } finally {
            dbconn.closeConnection(conn);
        }

        return jobID;
    }
    
}
