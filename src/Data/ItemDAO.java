/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

/**
 *
 * @author Niroshan
 */
public class ItemDAO 
{
    DBConnManager dbconn;
    public ItemDAO()
    {
        dbconn = new DBConnManager();
    }
    /*
     * this method is used  for insert Item to the database 
     */
    public boolean insertItem(Item it)
    {
        boolean saved = false;
        String item_id ;
        String description;
        double quantity;
        BigDecimal price;
        
               
        
        Connection conn = null;
            
        try 
        {
            item_id = it.getItem_ID();
            description = it.getDescription();
            quantity = it.getQuantity();
            price = it.getPrice();
            
            conn = dbconn.getConnect();
            Statement stmt = conn.createStatement();
            String sql = "INSERT INTO item (item_id,description,quantity,price,isDeleted)"
                    + "VALUES('"+item_id+"','"+description+"','"+quantity+"','"+price+"','0')";
            int i  = stmt.executeUpdate(sql);
            
            if(i>=1)
            {
                saved =true;
            }
            
        }
        catch (Exception e) 
        {
            System.out.println(e);
        }
        
        
        
        return saved;
    }
    /* this method is used to get the item is there or no
     *used in inserting item , updating Item and deleting Item
     */
    public boolean existingItem(String ItemID)
    {
        boolean existing = false;
       
        
        Connection conn = null;
        
        try 
        {
            conn = dbconn.getConnect();
            String sql = "SELECT COUNT(item_id) FROM item WHERE item_id = '"+ItemID+"'";
            Statement stmt = conn.createStatement();
            ResultSet rst = stmt.executeQuery(sql);
            if(rst.next() &&  Integer.parseInt(rst.getString(1)) >= 1 )
            {
                existing = true;
            }
        }
        catch (Exception e)
        {
            System.out.println(e);
        }
        finally
        {
            dbconn.closeConnection(conn);
        }
        
        
        
        return existing;
    }
    /*
     * this method is used for load the whole Item data to the Item table
     */
    public Vector loadItem()
    {
         Vector  itemDetails = new Vector() ;
        Connection conn = null;
        
        try 
        {
            conn = dbconn.getConnect();
            Statement stmt = conn.createStatement();
            String sql = "SELECT item_id,description,quantity,price FROM item WHERE isDeleted = 0 ";
            
            ResultSet rst = stmt.executeQuery(sql);
            
            
            while(rst.next())
            {
                Vector v = new Vector();
                v.add(rst.getString(1));
                v.add(rst.getString(2));
                v.add(rst.getString(3));
                v.add(rst.getString(4));
                itemDetails.add(v);
            }
        }
        catch (Exception e) 
        {
            System.out.println(e);
        }
        finally
        {
            dbconn.closeConnection(conn);
        }
        
        return itemDetails;
        
    }
    /*
     * update Item Details 
     */
    public boolean updateItemDetails(Item it)
    {
        boolean updated = false;
        Connection conn = null;
        
        try 
        {
            conn = dbconn.getConnect();
            Statement stmt = conn.createStatement();
            
            String itemID = it.getItem_ID();
            String description = it.getDescription();
            double quantity = it.getQuantity();
            BigDecimal price = it.getPrice();           
            String sql = "UPDATE item SET description = '"+description+"',quantity = '"+quantity+"',"
                    + "price = '"+price+"' WHERE item_id = '"+itemID+"' ";
            int i = stmt.executeUpdate(sql);
            
            if(i>=1)
            {
                updated = true;
            }
        }
        catch (Exception e) 
        {
            System.out.println(e);
        }
        finally
        {
            dbconn.closeConnection(conn);
        }
        
        return updated;
    }
    /*
     * this method is used for Delete selected Item.
     */
    public boolean deleteItem(String itemID)
    {
        boolean deleted = false;
        Connection conn = null;
        
        try 
        {
            conn = dbconn.getConnect();
            Statement stmt = conn.createStatement();
            
            String sql = "UPDATE item SET isDeleted = '1'  WHERE item_id = '"+itemID+"' ";
            
            int i = stmt.executeUpdate(sql);
            
            if(i>=1)
            {
                deleted = true;
            }
            
        }
        catch (Exception e) 
        {
            System.out.println(e);
        }
        finally
        {
            dbconn.closeConnection(conn);
        }
        
        return deleted;
    }
    public Vector searchItem(String itemID)
    {
        Vector itemDet = new Vector();
        Connection conn = null;
        
        try 
        {
            conn = dbconn.getConnect();
            Statement stmt = conn.createStatement();
            String sql = "SELECT item_id,description,quantity,price FROM item WHERE isDeleted = 0 AND item_id LIKE '"+itemID+"%' ";
            
            ResultSet rst = stmt.executeQuery(sql);
            
            
            while(rst.next())
            {
                Vector v = new Vector();
                v.add(rst.getString(1));
                v.add(rst.getString(2));
                v.add(rst.getString(3));
                v.add(rst.getString(4));
                itemDet.add(v);
            }
        }
        catch (Exception e) 
        {
            System.out.println(e);
        }
        finally
        {
            dbconn.closeConnection(conn);
        }
       
        return itemDet;
    }
    

    public Vector loadItemID(String itemID)
    {
         Vector  itemDetails = new Vector() ;
        Connection conn = null;
        
        try 
        {
            conn = dbconn.getConnect();
            Statement stmt = conn.createStatement();
            String sql = "SELECT item_id FROM item WHERE isDeleted = 0 AND item_id LIKE '"+itemID+"%' ";
            
            ResultSet rst = stmt.executeQuery(sql);
            
            
            while(rst.next())
            {
                Vector v = new Vector();
              //  v.add(rst.getString(1));
                itemDetails.add(rst.getString(1));
            }
        }
        catch (Exception e) 
        {
            System.out.println(e);
        }
        finally
        {
            dbconn.closeConnection(conn);
        }
        
        return itemDetails;
        
    }
    public String getItemDescription(String itemID)
    {
        Connection conn = null;
        String description = "";
        
        try 
        {
           conn = dbconn.getConnect();
           Statement stmt = conn.createStatement();
           String sql = "SELECT description FROM item WHERE item_id = '"+itemID+"'";
            ResultSet rst = stmt.executeQuery(sql);
            
           if(rst.next() && rst.getString(1) != null)
           {
               description = rst.getString(1);
           }
           
        }
        catch (Exception e) 
        {
            System.out.println(e);
        }
        finally
        {
            dbconn.closeConnection(conn);
        }
        
        return description;
        
    }
    public Vector loadItemID()
    {
         Vector  itemDetails = new Vector() ;
        Connection conn = null;
        
        try 
        {
            conn = dbconn.getConnect();
            Statement stmt = conn.createStatement();
            String sql = "SELECT item_id FROM item WHERE isDeleted = 0  ";
            
            ResultSet rst = stmt.executeQuery(sql);
           

            while (rst.next()) {
                itemDetails.add(rst.getString(1));
            }
        }
        catch (Exception e) 
        {
            System.out.println(e);
        }
        finally
        {
            dbconn.closeConnection(conn);
        }
        
        return itemDetails;
        
    }

}

