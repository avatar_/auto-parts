/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

/**
 *
 * @author Niroshan
 */
public class UserDAO 
{
    DBConnManager dbconn;
    
    public UserDAO()
    {
        dbconn = new DBConnManager();
    }
    
    public boolean saveUser(User us)
    {
        boolean saved = false;
                
        Connection conn = null ;
        
        try 
        {
            conn = dbconn.getConnect();
            Statement stmt = conn.createStatement();
            String uID = us.getUserID();
            String fname = us.getF_name();
            String lname = us.getL_name();
            String password = us.getPassword();
            
            String sql = "INSERT INTO user(user_id,f_name,l_name,password,isDeleted)"
                    + " VALUES ('"+uID+"','"+fname+"','"+lname+"','"+password+"','0')";
            int i = stmt.executeUpdate(sql);
            
            if(i==0)
            {
                saved = false;
            }
            else if(i > 0)
            {
                saved = true;
            }
            
        }
        catch (Exception e) 
        {
            System.out.println(e);
        }
        finally
        {
            dbconn.closeConnection(conn);
        }
        
        return saved;
    }
    
    public boolean existingUser(String userName,String password)
    {
        boolean exisiting = false;
        
        Connection conn = null;
        
        try 
        {
            conn = new DBConnManager().getConnect();
            Statement stmt = conn.createStatement();
            
            String ePassword = new encodePassword().encode(password);
            
            String sql = "SELECT * FROM user WHERE user_id = '"+userName+"' AND "
                    + "password = '"+ePassword+"' AND isDeleted = '0' ";
            
            
            
            ResultSet rst = stmt.executeQuery(sql);
            if(rst.next() && rst.getString(1) != null)
            {
                 
                exisiting = true;
            }
        }
        catch (Exception e) 
        {
            System.out.println(e);
        }
        finally
        {
            dbconn.closeConnection(conn);
        }
       
        return exisiting; 
       
    }
    
    public boolean existingUser(String userName) {
        boolean exisiting = false;

        Connection conn = null;

        try {
            conn = new DBConnManager().getConnect();
            Statement stmt = conn.createStatement();


            String sql = "SELECT * FROM user WHERE user_id = '" + userName + "' AND isDeleted = '0'";
            System.out.println(sql);
            ResultSet rst = stmt.executeQuery(sql);
            if (rst.next() && rst.getString(1) != null) {
                 
                exisiting = true;
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            dbconn.closeConnection(conn);
        }

        return exisiting;

    }

    public Vector loadUserDetails()
    {
        Vector details = new Vector<>();
        
        Connection conn = null;
        
        try 
        {
            conn = dbconn.getConnect();
            Statement stmt = conn.createStatement();
            
            String sql = "SELECT user_id,f_name,l_name FROM user WHERE isDeleted = '0' ";
            
            ResultSet rst = stmt.executeQuery(sql);
            
            while(rst.next())
            {
                Vector v = new Vector();
                v.add(rst.getString(1));
                v.add(rst.getString(2));
                v.add(rst.getString(3));
                
                details.add(v);
            }
            
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            dbconn.closeConnection(conn);
        }
        
        
        
        
        
        return details;
    }
    
    public boolean deleteUser(String pUserID) {
        boolean deleted = false;
        Connection conn = null;

        try {
            conn = dbconn.getConnect();
            Statement stmt = conn.createStatement();

            String sql = "UPDATE user SET isDeleted = '1' WHERE user_id = '" + pUserID + "' ";

            int i = stmt.executeUpdate(sql);

            if (i >= 1) {
                deleted = true;
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            dbconn.closeConnection(conn);
        }
        return deleted;
    }
    
    public boolean updateUser(User puser)
    {
        boolean updated = false;
        Connection conn = null;
        try {
            conn = dbconn.getConnect();
            Statement stmt = conn.createStatement();
            String UID = puser.getUserID();
            String fname = puser.getF_name();
            String lname = puser.getL_name();
            
            String sql ="UPDATE user SET f_name ='"+fname+"' , l_name = '"+lname+"'"
                    + "WHERE user_id = '"+UID+"'";

            int  i = stmt.executeUpdate(sql);
            
            if(i>0)
            {
                updated = true;
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            dbconn.closeConnection(conn);
        }
        
        return updated;
    }
}
