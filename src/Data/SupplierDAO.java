/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import com.sun.rmi.rmid.ExecOptionPermission;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

/**
 *
 * @author Niroshan
 */
public class SupplierDAO
{
    DBConnManager dbconn;
    
    public SupplierDAO()
    {
        dbconn = new DBConnManager();
    }
    /*
     * this method is used for insert a supplier
     */
    public boolean  insertSupplier(Supplier sup)
    {
        boolean saved = false;
        Connection conn = null;
        
        try 
        {
            String sup_ID = sup.getSupplier_ID();
            String supName = sup.getSupplier_name();
            String telephone = sup.getTelephone();
            conn = dbconn.getConnect();
            Statement stmt = conn.createStatement();
            
            String sql = "INSERT INTO supplier(supplier_ID,supplier_name,telephone,isDeleted)"
                    + "VALUES ('"+sup_ID+"','"+supName+"','"+telephone+"','0') ";
            int i = stmt.executeUpdate(sql);
            if(i >= 1 )
            {
                saved = true;
            }
            
        } catch (Exception e) 
        {
            System.out.println(e);
        }
        finally
        {
            dbconn.closeConnection(conn);
        }
        
        
        
        
        return saved;
    }

    /*
     * this method is used for check already supplier is there or not
     */
    public boolean existingSupplier(String supplierID)
    {
        boolean existing = false;
        Connection conn = null;
        
        try 
        {
            conn = dbconn.getConnect();
            Statement stmt = conn.createStatement();
            String sql ="SELECT COUNT(*)  FROM supplier WHERE supplier_ID = '"+supplierID+"'  AND  isDeleted ='0' ";
            ResultSet rst = stmt.executeQuery(sql);
            
            if( rst.next() && Integer.parseInt(rst.getString(1)) == 1)
            {
                existing = true;
            }
        }
        catch (Exception e) 
        {
            System.out.println(e);
        }
        finally
        {
            dbconn.closeConnection(conn);
        }
        return existing;
    }
    
    /*
     * this method is used for delete supplier
     */
    public boolean deleteSupplier(String supplierID)
    {
        boolean deleted = false;
        
        Connection conn = null;
        
        try 
        {
            conn = dbconn.getConnect();
            Statement stmt = conn.createStatement();
            
            String sql = "UPDATE supplier SET isDeleted = '1' WHERE supplier_ID = '"+supplierID+"' ";
            
            int i = stmt.executeUpdate(sql);
            
            if(i >= 1)
            {
                deleted = true;
            }
            
        }
        catch (Exception e) 
        {
            System.out.println(e);
        }
        finally
        {
            dbconn.closeConnection(conn);
        }
                
        
        
        return deleted;
        
    }

    /*
     * this method is used for load the supplier details into supplier table
    */
    public Vector loadSUpplier() {
        Vector supDet = new Vector();
        Connection conn = null;

        try {
            conn = dbconn.getConnect();
            Statement stmt = conn.createStatement();
            String sql = "SELECT supplier_ID,supplier_name,telephone FROM supplier WHERE isDeleted = '0' ";

            ResultSet rst = stmt.executeQuery(sql);

            while (rst.next()) {
                Vector v = new Vector();
                v.add(rst.getString(1));
                v.add(rst.getString(2));
                v.add(rst.getString(3));

                supDet.add(v);
            }

        } catch (Exception e) {
            System.out.println(e);
        } finally {
            dbconn.closeConnection(conn);
        }
        return supDet;
    }
    /*
     * this method is used for update supplier details
     */
    public boolean  updateSupplierDetails(Supplier sup)
    {
        boolean updated = false;
        Connection conn = null;
        
        try 
        {
            String supID = sup.getSupplier_ID();
            String supName = sup.getSupplier_name();
            String tele = sup.getTelephone();
            
            conn = dbconn.getConnect();
            Statement stmt = conn.createStatement();
            
            
            String sql = "UPDATE supplier SET supplier_name ='"+supName+"',telephone = '"+tele+"'"
                    + "WHERE supplier_ID = '"+supID+"'";
            
            int i = stmt.executeUpdate(sql);
            if(i >= 1 )
            {
                updated = true;
            }
        }
        catch (Exception e) 
        {
            System.out.println(e);
        }
        finally
        {
            dbconn.closeConnection(conn);
        }
        
        
        
        return updated;
    }
    
    /*
     * this method is used for oad the supplier details for combo box in buy tab
     */
    public Vector loadSUppliertoCombo()
    {
        Vector supDet = new Vector();
        Connection conn = null;
        
        try 
        {
            conn = dbconn.getConnect();
            Statement stmt = conn.createStatement();
            String sql = "SELECT supplier_ID FROM supplier WHERE isDeleted = '0' ";
            
            ResultSet rst = stmt.executeQuery(sql);
             
            while(rst.next())
            {
                supDet.add(rst.getString(1));
                          
            }
            
        }
        catch (Exception e) 
        {
            System.out.println(e);
        }
        finally
        {
            dbconn.closeConnection(conn);
        }
     
        return supDet;
    }
    
    /*
     * this method is used for search the supplier by supplier ID
     */
    public Vector searchSupplier(String supplierID) {
        Vector supDet = new Vector();
        Connection conn = null;

        try {
            conn = dbconn.getConnect();
            Statement stmt = conn.createStatement();
            String sql = "SELECT supplier_ID,supplier_name,telephone FROM supplier WHERE isDeleted = '0'"
                    + " AND supplier_ID LIKE '"+supplierID+"%' ";

            ResultSet rst = stmt.executeQuery(sql);

            while (rst.next()) {
                Vector v = new Vector();
                v.add(rst.getString(1));
                v.add(rst.getString(2));
                v.add(rst.getString(3));

                supDet.add(v);
            }

        } catch (Exception e) {
            System.out.println(e);
        } finally {
            dbconn.closeConnection(conn);
        }
        return supDet;
    }
}
