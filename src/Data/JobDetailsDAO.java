/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Vector;

/**
 *
 * @author Niroshan
 */
public class JobDetailsDAO 
{
    DBConnManager dbconn;
    
    public JobDetailsDAO()
    {
        dbconn = new DBConnManager();
    }
    
    public boolean saveJobDetails(JobDetails jobDT) {

        boolean saved = false;
        Connection conn = null;
        try {
            conn = dbconn.getConnect();
            Statement stmt = conn.createStatement();

            Vector jobdetails = jobDT.getJobdetails();

           
            for (int i = 0; i < jobdetails.size(); i++) {
                Object details[] = jobdetails.toArray();
                Vector Sdetails = (Vector) details[i];

                int jobID = Integer.parseInt(Sdetails.elementAt(0).toString());
                String itemID = Sdetails.elementAt(1).toString();
                int quantity = Integer.parseInt(Sdetails.elementAt(2).toString());

                String sql = "INSERT INTO sell_det(sell_id,itemID,quantity) "
                        + "VALUES (" + jobID + ",'" + itemID + "'," + quantity + " )";
                stmt.addBatch(sql);
                System.out.println(sql);
            }
            
             
            int[] k = stmt.executeBatch();

            if (k[0] >= 1) {
                saved = true;
            }

        } catch (Exception e) {
            System.out.println(e);
        } finally {
            dbconn.closeConnection(conn);
        }
        return saved;
    }
}
