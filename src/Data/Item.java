/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import java.math.BigDecimal;

/**
 *
 * @author Niroshan
 */
public class Item 
{
    private String item_ID;
    private String description;
    private double quantity;
    private BigDecimal price;
    private String isDeleted;
    
    public Item(String pItemID,String pDescription,double  pQuantity,BigDecimal pPrice)
    {
        this.item_ID = pItemID;
        this.description = pDescription;
        this.quantity = pQuantity;
        this.price = pPrice;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getItem_ID() {
        return item_ID;
    }

    public void setItem_ID(String item_ID) {
        this.item_ID = item_ID;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }
    
    
}
